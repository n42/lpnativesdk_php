#!/usr/bin/php
<?php

require('../LP/Logger.php');

use LP\Logger as Logger;

// setup the $log object, note you will need the correct installation Id and logServer url to use

$log = new Logger('123456789ABCDEFGH1234121', 'http://pp.lp4.io');

/*

// Log events for one user on day resolution
$apiLog = $log->apiLog('UserEvents');
$apiLog->key('user');
$apiLog->set('user','user@linkpulse.com');
$apiLog->set('event', 'User did something');
$apiLog->send();


// same as above
$log->apiLog('UserEvents')->key('user')->set('user','user@linkpulse.com')->set('event', 'User did something')->send();


// log event on daylevel
$log->apiLog('UserEvents')
->resolution(\LP\LogType\API::RESOLUTION_DAY)
->key( array('objectId', 'userId','model','id') )
->set('objectId','1224')
->set('userId','412412412')
->set('id',1234)
->set('email','user@linkpulse.com')
->set('ticket', '')
->set('model', 'campaign')
->set('changes', 'XXXX somethind changed')
->send();



// log an event with count
$log->apiLog('EventCounter')->key('eventId')->set('eventId','uniqueEvent')->inc('count',1)->send();
exit;
*/

// log event minuteLevel with custom date
$log->apiLog('UserEvents')
->resolution(\LP\LogType\API::RESOLUTION_DAY)
//->dateTime('2016/12/31 23:44:24')
->key( array('objectId', 'userId','model','id') )
->set('objectId','1224')
->set('userId','412412412')
->set('id',1234)
->set('email','user@linkpulse_com')
->set('ticket', '')
->set('model', 'campaign')
->set('changes', 'XXXX somethind changed')
->send();




