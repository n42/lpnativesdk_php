<?php
namespace LP;
/**
 * Linkpulse Log API
 *
 * Log Events to Linkpulse log servers
 *
 * @author nils@linkpulse.com
 * @date 20171017 Complete rewrite to support latest logServer version
 *
 */
require('LogType/API.php');

class Logger {

 
    private $installationId = '';
    private $logServer      = '';

    private $logData = array();

    /**
     * Constructor
     *
     * @param string installationId (ask us for this value)
     * @param string logServer      (ask us for this value)
     */
    public function __construct( $installationId, $logServer ) {
        if( is_null($installationId) || $installationId == '' || strlen($installationId) != 24) {
            throw new Exception("Invalid Installation Id");
        }
        if( is_null($logServer) || $logServer == '' || ( filter_var($logServer, FILTER_VALIDATE_URL) === false) ) {
            throw new Exception("Invalid logserver url given: '".$logServer."'");
        }
        $this->installationId = $installationId;
        $this->logServer = $logServer;
    }

    /**
     * Get a new LP\LogType\API 
     *
     * @param string logname (name of the dataset)
     */
    public function apiLog($logName) {
        return new \LP\LogType\API($this,$logName);
    }
 

    /**
     * Flush any unsent logdata
     *
     * @param array logData ( not documented )
     */
    public function addLogData($logData) {

        $validLogTypes = array(
            'x', // custom logApi
            //'p', // pageview
            // 'c', // clicks
            );

        if( empty($logData['logType']) || !in_array($logData['logType'],$validLogTypes) ) {
            throw new \Exception('Missing valid logType');
        }
        $logType = $logData['logType'];
        unset( $logData['logType'] );
        if( empty( $this->logData[$logType] ) ) {
            $this->logData[$logType] = array();
        }
        $this->logData[$logType][]=$logData;

    }
    
    /**
     * Flush any unsent logdata
     *
     * @param string logtype (not implemented)
     */
    public function sendLogs($sendLogType = '') {

        $curlHandles = array();
        foreach( $this->logData as $logType => $logData ) {
            if( $sendLogType == '' || $sendLogType == $logType ) {
                foreach($logData as $data) {
                    //print "SENDING: ".$this->logServer.'/'.$logType.'?i='.$this->installationId.'&'.http_build_query($data)."\n";
                    $ch = curl_init( $this->logServer.'/'.$logType.'?i='.$this->installationId.'&'.http_build_query($data) );
                    //curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                    $curlHandles[]=$ch;
                }
                
            }
        }
        $this->logData = array();
        if( sizeof($curlHandles) > 0) {
            $multiHandler = curl_multi_init();
            foreach( $curlHandles as $ch ) {
                curl_multi_add_handle( $multiHandler, $ch);
            }
            
            $run = null;
            do {
                curl_multi_exec($multiHandler,$run);
                /*
                  Error handling will be supported when PHP 7 is becoming common
                  $status = curl_multi_exec($multiHandler,$run);
                  if( $status > 0) {
                    print "ERROR!\n " . curl_multi_strerror($status);
                  }
                */
            } while($run);

            foreach( $curlHandles as $ch ) {
                curl_multi_remove_handle($multiHandler,$ch);
                curl_close($ch);
            }
            curl_multi_close($multiHandler);
            return true;
        }
        return false;
    }
    


}



