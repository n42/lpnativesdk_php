<?php
namespace LP\LogType;
require('AbstractLogType.php');
/**
 * Linkpulse LogType for API
 *
 * Log API KPi's to Linkpulse log servers
 *
 * @author nils@linkpulse.com
 * @date 20171017 Complete rewrite to support latest logServer version
 *
 */
class API extends \LP\LogType\AbstractLogType {

    const RESOLUTION_DAY    = 'd';
    const RESOLUTION_HOUR   = 'h';
    const RESOLUTION_MINUTE = 'm';
    
    private $logName = '';
    private $resolution = '';
    private $key  = '';
    private $incs = array();
    private $sets = array();
    private $adds = array();
   

    public function __construct($logHandler, $logName, $resolution = self::RESOLUTION_DAY) {
        parent::__construct($logHandler);
        $this->logName = $logName;
        $this->resolution = $resolution;
    }
    
    /**
     * which resolution to store the data in
     *
     * @param RESOLUTION_DAY|RESOLUTION_HOUR|RESOLUTION_MINUTE (default RESOLUTION_DAY);
     */
    public function resolution($res) {
        if( $res == self::RESOLUTION_DAY || $res == self::RESOLUTION_HOUR || $res == self::RESOLUTION_MINUTE) {
            $this->resolution=$res;
            return $this;
        }
        throw new \Exception("Unknown resolution given: $res");
    }

     /**
     * Which values should form the key for this dataset
     *
     * @param array|string one or more names of variables in the dataset
     */
    public function key($keys) {
        if( isset( $keys ) ) {
            $this->key = $keys;
        }
     
        return $this;
    }

    /** 
     * Set dateTime for when this event should be logged on
     *
     * @param string dateTime (see valid formats here: http://php.net/manual/en/datetime.formats.php);
     */
    public function dateTime( $dateTime ) {
        if( strtotime($dateTime) === false) {
                 throw new \Exception("Could not dateTime($dateTime), invalid dateTime format");
            }
        $this->sets['dateTime']=$dateTime;
        return $this;
    }
    
    /**
     * Increment given variabel with given numeric value
     *
     * @param string key name ([a-z][a-z0-9\_\-]{2,24} 
     * @param number increment value 
     */
    public function inc($key,$value) {
        if($this->testKey($key)) {
            if( is_float($value)  ) {
                $this->incs[$key]=(float) $value;
            }
            else if( is_int($value) ) {
                $this->incs[$key]=(int) $value;
            }
            else {
                throw new \Exception('Unknown value given: ($key) $value');
            }
        }
        return $this;
    }
    
    /**
     * Add value to a dataset (if not already in the dataset)
     *
     * @param string key name ([a-z][a-z0-9\_\-]{2,24} 
     * @param string value 
     */
    public function add($key,$value) {

        if( !isset($value) || $value == '') {
            throw new \Exception("Can not add an empty value");
        }
        if($this->testKey($key) && $value != '' ) {
            if(empty($this->adds[$key]) || !is_array( $this->adds[$key] ) ) {
                $this->adds[$key] = array();
            }
             $this->adds[$key][]=$value;
        }
        else {
            throw new \Exception("Could not 'add($key,$value)'");
        }
        return $this;
    }

    /**
     * Set/update given variable a value (will be added if not existing)
     *
     * @param string key name ([a-z][a-z0-9\_\-]{2,24} 
     * @param string value 
     */
    public function set($key,$value) {
        if( strtolower($key) == 'datetime' ) {
            throw new \Exception("Could not use key 'dateTime', it is a reserved keyword");
        }
        
        if( !isset($value) ) {
            throw new \Exception("Can not set an empty value");
        }
      
        if($this->testKey($key)) {
            $this->sets[$key]=$value;
         }
         else {
            throw new \Exception("Could not set($key,$value)");
        }
         return $this;
    }

    protected function compileLogData() {

        if(empty($this->sets['dateTime'])) {
            $this->sets['dateTime']=date(\DateTime::ATOM);
        }
        
        $tmpKeyValues = array();
        
        
        $compile = function($data, &$tmp) {
            $newData = array();
            foreach( $data as $k => $v) {
                $tmp[$k]=1;
                if( is_array($v) ) {
                    array_walk($v, function(&$item) { $item = urlencode($item); });
                    $newData[]=$k.':'.implode(',',$v);
                }
                else {
                    $newData[]=$k.':'.urlencode($v);
                }
            }
            return implode('|',$newData);
        };


        $logData = array(
            'logType' => 'x',
            'res' => $this->resolution,
            'log' => $this->logName,
            'key' => (is_array($this->key) ? implode('~', $this->key) : $this->key),
            'inc' => $compile($this->incs, $tmpKeyValues),
            'set' => $compile($this->sets, $tmpKeyValues),
            'add' => $compile($this->adds, $tmpKeyValues),
            );


        $missingKeyValues =  array_diff( (!is_array($this->key) ? array($this->key) : $this->key), array_keys( $tmpKeyValues) );
        if( sizeof( $missingKeyValues ) > 0) {
            throw new \Exception('Missing key values for one or more keys, missing for: '.implode(', ',$missingKeyValues));
                              
        }
        return $logData;
    }

    private function testKey($value) {
        if(preg_match('/^[a-z][a-z0-9\_\-]{1,24}$/i',$value) == 1) {
            return true;
        }
        return false;
    }
}