<?php
namespace LP\LogType;
/**
 * Linkpulse LogType for API
 *
 * Log API KPi's to Linkpulse log servers
 *
 * @author nils@linkpulse.com
 * @date 20171017 Complete rewrite to support latest logServer version
 *
 */

abstract class AbstractLogType {

    protected $logHandler;

    public function __construct($logHandler) {
        $this->logHandler = $logHandler;
    }
    
    abstract protected function compileLogData();


    /*
     *  Send the log data
     *
     *  @param none
     */
    public function send() {
        $this->store();
        $this->logHandler->sendLogs();
        
    }
    /*
     * Store the logdata and flush at later stage
     *      
     *  @param none
     */
    public function store() {
        $logData = $this->compileLogData();
        $this->logHandler->addLogData($logData);
    }
 
    public function __toString() {
        return get_class($this);
    }
}