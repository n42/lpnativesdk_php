# Linkpulse NATIVE SDK for PHP

See bin/example.php

Currently only customlog API is implemented.


Composer settings:
```
"require": {
       "linkpulse/logger": "~1.0.0"
},
"repositories": [
 {
   "type": "vcs",
   "url":  "https://bitbucket.org/n42/lpnativesdk_php.git"
 }
]
```

Copyright Linkpulse - a plista company 2017